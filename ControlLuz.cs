﻿using UnityEngine;
using System.Collections;

public class ControlLuz : MonoBehaviour {

	public int lightFactor = 0;
	public int ValorIntercambio;
    public int CantAberturas;

   
    public Light controlledLight01 = null;

    public GameObject CamaraSky;
    public GameObject[] VidriosAb = new GameObject[16];/// Array de Ventanas y puertas con vidrio

    public bool ForceUpdateAberturas;
    

    Color ChangeVidrio;
    public Color Fondo;
    void Start () {

		ValorIntercambio = 0;
		Fondo = CamaraSky.GetComponent<Camera> ().backgroundColor;
	}


    void Update () {
        
		CantAberturas = GameObject.FindGameObjectsWithTag ("Aberturas").Length;
		if((VidriosAb.Length != CantAberturas) || ForceUpdateAberturas){
			VidriosAb = new GameObject[0];
			VidriosAb = GameObject.FindGameObjectsWithTag ("Aberturas");

			if(VidriosAb.Length>0){
				foreach(GameObject AberturaObj in VidriosAb){
					int largo=AberturaObj.GetComponent<MeshRenderer>().materials.Length;
					if(largo>0){
						foreach(Material MatAbObj in AberturaObj.GetComponent<MeshRenderer>().materials){
						//	Debug.Log ("====>>> Material= '"+MatAbObj.name+"'");
							if(MatAbObj.name=="VIDRIO (Instance)"){
								MatAbObj.color=Fondo;
							//	Debug.Log ("===========>>> Change color. Color="+Fondo);

							}
						}
					}
				}
			}
			
		}

		
	}



	public void Atardecer(){
		Light l = controlledLight01.GetComponent<Light>(); // Get the Light component
		Color c = new Color();
		Debug.Log ("LuzInicial:" + l.color);

        switch (ValorIntercambio) {


            case 0:
                //Cambiar color de luz
                c = new color(1.76f,0.62f ,0.36f ,0.55f );
                l.color = c;
			    l.intensity = 1f;
                //cambiar skybox
                Fondo = new Color(1.76f, 0.62f, 0.36f, 0);
                CamaraSky.GetComponent<Camera> ().backgroundColor = Fondo;

                Debug.Log ("color" + l.color);
			    ValorIntercambio = 1;

                break;

            case 1:
                //Cambiar color de luz
                c = new color(0.08f, 0.21f, 1.09f, 2.55f);
                l.color = c;
			    l.intensity = 1f;

                //cambiar skybox
                Fondo = new Color(0.21f, 0.44f, 0.71f, 0);
                CamaraSky.GetComponent<Camera> ().backgroundColor = Fondo;

                ValorIntercambio = 2;
			    Debug.Log("Valor de intercambio: "+ ValorIntercambio);

                break;

            case 2:
                //Cambiar color de luz
                c = new color(2.550f, 2.550f, 2.550f, 1.550f);
			    l.color = c;
			    l.intensity = 0.5f;
                //cambiar skybox
                Fondo = new Color(0.37f, 0.639f, 0.851f, 1);
                CamaraSky.GetComponent<Camera> ().backgroundColor = Fondo;

                ValorIntercambio = 0;
			    Debug.Log("Valor de intercambio: "+ ValorIntercambio);

                break;

		}

		ForceUpdateAberturas = true;

	}



}

		