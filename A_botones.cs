﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Linq;
using System.Collections.Generic;
public class A_botones : MonoBehaviour {

    //---------------------------------------------------------------------------
    //Este script corresponde a un menú de Ayuda en modo de acordeón desplegable.
    //---------------------------------------------------------------------------

    public GameObject Content;//Contenedor de botones y textos
	RectTransform ContentHeight;//Rect transform para utilizar el contenedor
	float InitialHeight;//Altura inicial del contenedor
	public GameObject[] Buttons;//Array de botones
	List<float> initPositionsList = new List<float>();//Lista de Posicion de los botones
	List<float> initPositionsTextList = new List<float>();//Lista de posición de textos
	float PosYInicial;//Posicion Inicial Y
	float Height;//variable auxiliar para la altura
	float PosYText;//variable auxiliar de posicion en Y del texto
	float HeightText;//Variable auxiliar para la altura del texto


	// Use this for initialization
	void Start () {
        //Seteo y almacenamiento inicial de botones y textos
        //Recorrido del largo del array de botones 
		for(int j = 0; j <Buttons.Length; j +=2){ //los números pares dentro del array corresponde a los botones
			PosYInicial = Buttons [j].GetComponent<RectTransform> ().anchoredPosition.y;//se setea la variable PosYInicial con la posición Y de cada botón 
            Height = Buttons [j].GetComponent<RectTransform> ().rect.height;//Se setea la variable Height con la altura del rec transform de cada  boton
			initPositionsList.Add (PosYInicial);//Se almacena la posicion Inicial en Y en la lista de Botones
	
		}
        //Recorrido del largo del array de textos 
        for (int j = 1; j <Buttons.Length; j +=2){//los números impares dentro del array corresponde a los textos
			PosYText = Buttons [j].GetComponent<RectTransform> ().anchoredPosition.y;//se setea la variable PosYText con la posición Y de cada texto
            HeightText = Buttons [j].GetComponent<RectTransform> ().rect.height;//Se setea la variable Height con la altura del rec transform de cada  texto
            initPositionsTextList.Add (PosYText);//Se almacena la posicion Inicial en Y en la lista de Textos

        }
        //Recorrido para debuguear array de botones y textos
        for (int j = 0; j < Buttons.Length; j++) {
			Debug.Log("initPositionsList["+j+"]" +initPositionsList[j]);
		}
		 InitialHeight = Content.GetComponent<RectTransform> ().rect.height;//Se setea la Altura Inicial con la altura del Inicial del Contenedor
	
	}

    //Metodo para activacion y desactivación de despliegue de textos en base a botones
    public void OnOffText(int index){
		
		int TextOfButton = index +1;// TextOfButton corresponde al nro del texto en el Array de textos. Se suma +1 ya que el valor index corresponde al nro de botón clickeado.
        float SelectedButtonHeight;// variable de altura del boton seleccionado
		float TextOfButtonHeight;// variable de altura del texto a desplegar
        SelectedButtonHeight = Buttons [index].GetComponent<RectTransform> ().rect.height;//se asigna el valor a la variable SelectedButtonHeight con el balor del botón
        TextOfButtonHeight = Buttons [index + 1].GetComponent<RectTransform> ().rect.height;//se asigna el valor a la variable TextOfButtonHeight con el balor del texto

        if (Buttons [TextOfButton].activeSelf == false)//Validar si el texto se encuentra inactivo.
        {
			ResetPosAll (index);// se resetean todas las posiciones.------------

			Buttons [TextOfButton].SetActive (true);//se activa el texto
			
            //activa el texto y mueve la posicon de los otros botones
            ContentHeight = Content.GetComponent<RectTransform> ();//se asigna el rect transdorm a content Height

            float newHeight = Content.GetComponent<RectTransform> ().rect.height + TextOfButtonHeight;//se crea una variable auxiliar para la altura y se toma el valor del contenedor + la alturaa del texto activo

			ContentHeight.sizeDelta = new Vector2(Content.GetComponent<RectTransform> ().rect.width, newHeight);// Se ajusta el contenedor a la nueva altura.

            //recorrido de los botones siguientes al seleccionado
			for (int i = index +2 ; i < Buttons.Length; i += 2) {
				
				if (i == index + 2) {//se valida que sean los botones siguientes al seleccionado

					//Primer boton para setear con el alto del panel del texto
					TextOfButtonHeight = Buttons [i - 1].GetComponent<RectTransform> ().rect.height;
				    //se setea la nueva posición de los botones siguientes.
					Buttons [i].GetComponent<RectTransform> ().anchoredPosition = new Vector2 (Buttons [i].GetComponent<RectTransform> ().anchoredPosition.x, Buttons [i].GetComponent<RectTransform> ().anchoredPosition.y - TextOfButtonHeight);
						
				} else {
					
                    //se setea la posicions del resto de los botones.
					Buttons [i].GetComponent<RectTransform> ().anchoredPosition = new Vector2 (Buttons [i].GetComponent<RectTransform> ().anchoredPosition.x, Buttons [i].GetComponent<RectTransform> ().anchoredPosition.y -TextOfButtonHeight );
				}
					
			}

		} else {
                   
			Buttons [TextOfButton].SetActive (false);//Desactiva el panel de texto 
            ResetPosAll (index);// resetea la posicion del resto de los botones a la posicion inicial

        }
    }

    //Metodo de reseteo de la posicion de os botones y tamaño del contenedor
	public void ResetPosAll(int index){
		ContentHeight = Content.GetComponent<RectTransform> ();// se toma el rectransform del contenedor
		ContentHeight.sizeDelta = new Vector2(Content.GetComponent<RectTransform> ().rect.width, InitialHeight);//se pisiciona a la posicion inicial
        
        //Se recorren todos los textos que fueron desplazados en Y y se desactivan
        for (int iI = 1; iI < Buttons.Length; iI += 2) {
			Buttons [iI].SetActive (false);
		}

        //se recorren los botones y se retornan a la posicion inicial.
		for (int i = 0; i< Buttons.Length; i+=2) {
			//	2-4-6-8-10-12-14
			//	0-1-2-3-4-5-6-7-8-9
			int ValueToSetListObject = i/2; //esta variable sirve para encontrar la posicion en el array de posiciones iniciales en y de los botones.
			float posYSet = initPositionsList [ValueToSetListObject];// se busca la posicion inicial en Y de cada boton
			Buttons [i].GetComponent<RectTransform> ().anchoredPosition = new Vector2 (Buttons [i].GetComponent<RectTransform> ().anchoredPosition.x, posYSet);//se posiciona el botón a la posicion inicial que tenía al inicio.

		}
	}
    
    //Fin de la clase
}
