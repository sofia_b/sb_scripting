﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using SimpleJSON;
using UnityEngine.UI;
public class ValidacionInicialAlmacenado : MonoBehaviour {
  
    GameObject PersistentObject;// GameObject que se va a mantener persistente
    public GameObject LoadingEffect;// Gameobject con animación de Loading
    public GameObject Alertmsisdn;//GAmeObject de mensaje de msisdn inválido

    string msisdnGuardado;// Variable para almacenamiento de msisdn
    public string msisdnv;// variable publica para nro msisdn validado

    bool validacionInicial;//booleano para validacion inicial

    //Input para Verificar cantidad de nros ingresados msisdn
    public InputField msisdnValidacion;
    
 

    // Use this for initialization
    void Start() {
     
        PersistentObject = GameObject.FindGameObjectWithTag("PersistentObject");//buscar por tag y asignar Gameobject  
        
        //Recuperación de msisdn guardado en telefono
        if (SceneManager.GetActiveScene().name == "Splash")//Validación de Escena 
        {
            Validarmsisdn();
        }

        elapsedTime = 0;//se reinicia el valor de conteo de tiempo
    }
    public bool msisdnAlmacenado;//booleano para validacion de almacenamiento
    public bool msisdnValido;//bolleano para validacion
    public GameObject mediaPlayerController;//GameObject para asignación

    //Metodo para asignacion de nro msisdn y llamada a validación
    public void Validarmsisdn()
    {
        if (PersistentObject.GetComponent<DontDestroyInScenes>().guardado == true)//se llama al booleano "guardado" en el script del GAmeObject Persistente.
        {
            msisdnGuardado = PersistentObject.GetComponent<DontDestroyInScenes>().msisdnGuardado;//se asigna el nro msisdn guardado en el objeto persistente
            Debug.Log("1)msisdnGuardado= " + msisdnGuardado);//Debug para visualizar el resultado de la asignación anterior
            Validar(msisdnGuardado);//llamado al metodo de validación asignando el nro msisdn
        }
        
        
       
    }
  
    //ValidacionINICIAL para Escena Splash
    void Validar(string arg) {
        validacionInicial = true;
        StartCoroutine(ValidarMSISDN());// se inicia la corrutina para validacion del nro msisdn
    }


   //método para validación de nro msisdn
    public void ValidarNRO(string arg)
    {
        //Validación contra URl de la api
       
        validacionInicial = false;
        Debug.Log("validacionInicial:" + validacionInicial.ToString());

        if (validacionInicial == true)
        {
            Debug.Log("validacionInicial- Persistent: " + validacionInicial);
            //asignación de Url para llamada a la api
            urlValidarMSISDN = "http://n.smartmob.mobi/nsmart.asmx" + "/control_suscripcion?msisdn=" + PersistentObject.GetComponent<DontDestroyInScenes>().msisdnGuardado + "&idProducto=1";//??? id del producto es variable??
            StartCoroutine(ValidarMSISDN());
        }
        else
        {
            Debug.Log("validacionInicial: " + validacionInicial);
            //Validar cantidad de characters
            if (msisdnValidacion.text.Length < 8)// no pueden ser menos de 8 nros
            {
                Debug.Log("Faltan digitos. Es menor a 8: " + msisdnValidacion.text.Length);//debug para conocer la cantidad de caracteres ingresados
                              
                LoadingEffect.gameObject.GetComponent<LoadingSpin>().loading = false;//se desactiva loading
                Alertmsisdn.SetActive(true);//se activa mensaje de error de cantidad de caracteres.  
                //}
            }
            else
            {
                //asignación de Url para llamada a la api
                urlValidarMSISDN = "http://n.smartmob.mobi/nsmart.asmx" + "/control_suscripcion?msisdn=" + msisdnValidacion.text + "&idProducto=1";//??? id del producto es variable??
                StartCoroutine(ValidarMSISDN());//comienzo de corrutina para validacion.
            }

        }
       
        
       
      
    }
 
    string urlValidarMSISDN;
   
    //Ienumerator común para Escena Splash y para Escena Verificar nro msisdn
    IEnumerator ValidarMSISDN() {
            
        
        //Debug.Log("urlGETValidarMSISDN : " + urlValidarMSISDN);//Debug para ver si el valor que traer la url es correcto
        
        //llamado y descarga de info 
        WWW www = new WWW(urlValidarMSISDN);
        //Respuesta de API
        yield return www;
                   
        //Validacion de la descarga de informacion de la api
        if (!string.IsNullOrEmpty(www.error))
        {
            print(www.error);//Se muestra en consola que hubo error en la descarga de la informacion de la api
        }
        else
        {
           
            string CutUsuario = www.text;// asignacion de resultado a variable auxiliar
            string CutInit = CutUsuario.Remove(0, 82);//recorte de caracteres del resultado 
            Debug.Log("CutInit: " + CutInit);

            //---- Resultados recibidos de los servicios

            //usuario sin suscripcion</string>
            //Usuario con suscripcion</string>
            //usuario sin suscripcion o invalido</string>

            //condicional para resultado y asignacion de validacion válida/invalida en msisdnValido
            switch (CutInit)
            {

                case "usuario sin suscripcion</string>":
                    msisdnValido = false;
                    break;
                case "usuario sin suscripcion o invalido</string>":
                    msisdnValido = false;
                    break;
                case "Usuario con suscripcion</string>":
                    msisdnValido = true;
                    PersistentObject.GetComponent<DontDestroyInScenes>().msisdnGuardado = msisdnValidacion.text;//se almacena el texto del input en el objeto persistente.
                   PlayerPrefs.SetString("msisdnGuardado", msisdnValidacion.text);// se almacena el valor en la memoria.
                    
                    break;
                case "usuario suscripto</string>":
                    msisdnValido = true;
                    PersistentObject.GetComponent<DontDestroyInScenes>().msisdnGuardado = msisdnValidacion.text;//se almacena el texto del input en el objeto persistente.
                    PlayerPrefs.SetString("msisdnGuardado", msisdnValidacion.text);// se almacena el valor en la memoria.
                    break;
                    //usuario no suscripcion</string>
                    
                case "usuario no suscripcion</string>":
                    msisdnValido = false;
                    break;
            }

            Debug.Log("msisdnValido: " + msisdnValido.ToString());
            //se desactiva el loading, si no es nulo, luego de la validación con los servicios
            if (LoadingEffect != null)
            {
                LoadingEffect.gameObject.GetComponent<LoadingSpin>().loading = false;
            }
        }

        sendToNextScene();// se pasa a la siguiente escena

    }

  
    //------UPDATE-----
    
    bool doIt = false;
    public bool SmartR;
    public GameObject PersistentMain;
    private float elapsedTime;
    private void Update()
    {
      //  Debug.Log("SceneManager.GetActiveScene().name = " + SceneManager.GetActiveScene().name);
        if (SceneManager.GetActiveScene().name == "Splash")//validacion de escena activa
        {
            // Validación de finalizacion de video de splash para pasar a la siguiente escena dependiendo de si fue validado o no el msisdn
            if (doIt == false)
            {
                PersistentMain = GameObject.FindGameObjectWithTag("PersistentObject");
                SmartR = PersistentMain.GetComponent<DontDestroyInScenes>().SmartRMode;
                elapsedTime += Time.deltaTime;// conteo de segundos 
                Debug.Log("elapsedTime: " + elapsedTime);
                // se requieren 8 segundos para poder pasar a la siguiente escena luego de que termina el video splash
                if (elapsedTime > 8)
                {
                    if (SmartR == true)
                    {
                        Debug.Log("EndVideo= true");
                        //Luego de la validación se dirige hacia la escena correspondiente
                        sendToNextScene();
                    }
                    doIt = true;
                }
            }
        }
    }

    //Metodo para paso a siguiente escena
    void sendToNextScene() {
     
        //Luego de la validación se dirige hacia la escena correspondiente
        if (msisdnValido == true)//Si es true es porque el usuario se encuentra registrado en el sistema
        {
            Debug.Log("msisdnValido= true");
            SceneManager.LoadScene(4);

        } else//Usuario no se encuentra registrado en el sistema.
        {
          Debug.Log("Entra en validar escena Nro");
          if (SceneManager.GetActiveScene().name == "1ValidacionServicio")
          {
            SceneManager.LoadScene(2);
          }else{
            Debug.Log("msisdnValido= false");
            SceneManager.LoadScene(1);
          }
        }        
    }
    
    //FIn de la clase
}
