using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.Catalogue;
using System;
using UnityEngine.UI;
using UnityEngine.EventSystems;


public class BotonesSetsColores: MonoBehaviour{

//Clase para creación y funcionalidad de botones de cada variante de color del modelo 3d descargado. Productos.

	public string SNombres;
	public string IdSetsS;
	public string TextoSets;
	public string IdSets;
	public string[] NombresTxt;
	public string[] CatMatName;
	
	public int NroSet;
	public int CantSetsMat;
	public int SetSeleccionado;
	
	Text TextoBoton;
	public Text TextoBotonSets;
	public Font myFont;
	
	public Texture textura1;
	public Texture textura2;
	public Material Pmaterial;
	public Material titomaterial;

	GameObject SMData;
	public GameObject SM;
	public GameObject PanelBotones;
	
	SceneManager ManagerScript;
	public CatalogueItem Item;
	public CatalogueItemVariation var;

	//---------------------
	
	void Start(){
		NroSet = 0;
	}

	void Update()
	{
		CantSetsMat = SM.GetComponent<SceneManager>().NroSetsSelected;
		IdSetsS = SM.GetComponent<SceneManager>().idSelected;
		TextoSets = SM.GetComponent<SceneManager> ().nombreSet;

		if (IdSets != IdSets) {
			GetNroMat();	
		}
		
	}

	public void GetNroMat(){

		//--Destruye botones 
		foreach(Component child in PanelBotones.GetComponentsInChildren<Button>())
		{
			DestroyObject(child.gameObject);
		}

		CantSetsMat = SM.GetComponent<SceneManager>().NroSetsSelected;
		IdSetsS = SM.GetComponent<SceneManager>().idSelected;
		//	Debug.Log ("IDSelected" +IdSets);
		TextoSets = SM.GetComponent<SceneManager> ().nombreSet;
		//Debug.Log ("TextoSelected" +TextoSets);
		SetsColores();
	}

	public void SetsColores(){

		for(int i = 0; i <CantSetsMat; i ++) {

			//Debug.Log ("BOTON " + i);
			// Create the EventSystem to handle events in the first place (this is required for the events on the canvas and only one is enough even if you have multiple canvases)
			new GameObject ("EventSystem", typeof(EventSystem), typeof(StandaloneInputModule));
	
			// Create button
			//Button button = (new GameObject ("Button", typeof(CanvasRenderer), typeof(Image), typeof(Button), typeof(Text))).GetComponent<Button> ();
			Button button = (new GameObject ("Button", typeof(CanvasRenderer), typeof(Button), typeof(Text), typeof(BoxCollider2D), typeof( ButtonSetsValue))).GetComponent<Button> ();
			// Assign the button to the canvas using hierarchy
			button.transform.parent = PanelBotones.transform;

			//button.tag = IdSets;
			// Add function to trigger on click

			// Place the button at the middle
			RectTransform buttontransform = button.GetComponent<RectTransform> ();
			buttontransform.localPosition = Vector2.zero;

		
			Text TextoBoton = button.GetComponent<Text>();
		
			if(SM.GetComponent<SceneManager>().NombresA.Length>=i){
				TextoBoton.text = SM.GetComponent<SceneManager>().NombresA[i];
			}else{
				TextoBoton.text = "ERROR...";
			}

			button.GetComponent<ButtonSetsValue>().value = i;


			TextoBoton.font = myFont;
			//---
			TextoBoton.verticalOverflow= VerticalWrapMode.Overflow;

			//---
			BoxCollider2D BoxButton = button.GetComponent<BoxCollider2D>();
			button.GetComponent<Button>().onClick.AddListener(() => ChangeSet(button.GetComponent<ButtonSetsValue>().value));
			button.transition = Selectable.Transition.ColorTint;

			button.targetGraphic = TextoBoton;


			ColorBlock cb = button.colors;
			cb.highlightedColor = new Color( 0.5f, 0.5f, 0.5f, 1f);
       		button.colors = cb;

		}

	}

	public void ChangeSet(int valor)
	{
		SetSeleccionado = valor;
		//CatMatToUse
		SM.GetComponent<SceneManager> ().ChangeMaterialsSet (valor);
	}
//Fin de la clase

}