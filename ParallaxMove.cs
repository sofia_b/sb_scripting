﻿using UnityEngine;
using System.Collections;

public class ParallaxMove : MonoBehaviour
{

	public RectTransform MoveRT;
	public RectTransform [] BckRTPublic;
	private RectTransform [,] BckRTmtrx;

	public float Fact=1f;
	public float ScaleX=1f;
	public float ScaleY=1f;

	public int RowMx;
	public int ColMx;

	private float imgW;
	private float imgH;

	private Vector3 ActualPos;
	private Vector2 FinalPos;
	private int[] Traslade=new int[2];

    void Start()
    {
		BckRTmtrx = new RectTransform[RowMx, ColMx];
		if (BckRTPublic.Length == RowMx * ColMx) {
			for (int column = 0; column < ColMx; column++) {
				for (int row = 0; row < RowMx; row++) {
					BckRTmtrx [row, column] = BckRTPublic [row + (column * RowMx)];
				}
			}
			imgW = BckRTmtrx [0, 0].rect.width;
			imgH = BckRTmtrx [0, 0].rect.height;
		}
	}

    void Update()
    {

		FinalPos = (-MoveRT.anchoredPosition)/Fact;
		ActualPos = new Vector3 (FinalPos.x, FinalPos.y, 0);
		this.transform.position = ActualPos;

		int[] TrasladeTemp=new int[2];
		TrasladeTemp [0] = (int)(FinalPos.x / ScaleX);
		if ((FinalPos.y / ScaleY) > 0) {
			TrasladeTemp [1] = (int)((FinalPos.y / ScaleY)-0.1f);
		} else {
			TrasladeTemp [1] = (int)((FinalPos.y / ScaleY)+0.1f);
		}
		if (TrasladeTemp [0] != Traslade [0]) {
			if (TrasladeTemp [0] > Traslade [0]) {
				int max = Mathf.Abs (TrasladeTemp [0] - Traslade [0]);
				for (int i = 1; i <= max; i++) {
					Move_Left ();
					Traslade [0]++;
				}
			} else {
				int max = Mathf.Abs (Traslade [0] - TrasladeTemp [0]);
				for (int i = 1; i <= max; i++) {
					Move_Rigth ();
					Traslade [0]--;
				}
			}
		}

		if (TrasladeTemp [1] != Traslade [1]) {
			if (TrasladeTemp [1] > Traslade [1]) {
				int max = Mathf.Abs (TrasladeTemp [1] - Traslade [1]);
				for (int i = 1; i <= max; i++) {
					Move_Down ();
					Traslade [1]++;
				}
			} else {
				int max = Mathf.Abs (Traslade [1] - TrasladeTemp [1]);
				for (int i = 1; i <= max; i++) {
					Move_Up ();
					Traslade [1]--;
				}
			}
		}

	}

	private void Move_Up(){
		RectTransform Temp = new RectTransform();
		RectTransform Temp2 = new RectTransform();
		for (int column = 0; column < ColMx; column++) {
			Vector2 TempVect = new Vector2 (
				BckRTmtrx [RowMx-1, column].anchoredPosition.x,
				BckRTmtrx [0, column].anchoredPosition.y + imgH
			);
			BckRTmtrx [RowMx-1, column].anchoredPosition = TempVect;
		}
		for (int column = 0; column < ColMx; column++) {
			for (int row = 0; row < RowMx; row++) {
				if (row == 0) {
					Temp = BckRTmtrx [row + 1, column];
					BckRTmtrx [row + 1, column] = BckRTmtrx [row, column];
				} else if (row == RowMx - 1) {
					BckRTmtrx [0, column] = Temp;
				} else {
					Temp2 = Temp;
					Temp = BckRTmtrx [row + 1, column];
					BckRTmtrx [row + 1, column] = Temp2;
				}
			}
		}
	}

	private void Move_Down(){
		RectTransform Temp = new RectTransform();
		RectTransform Temp2 = new RectTransform();
		for (int column = 0; column < ColMx; column++) {
			Vector2 TempVect = new Vector2 (
				BckRTmtrx [0, column].anchoredPosition.x,
				BckRTmtrx [RowMx-1, column].anchoredPosition.y - imgH
			);
			BckRTmtrx [0, column].anchoredPosition = TempVect;
		}
		for (int column = 0; column < ColMx; column++) {
			for (int row = RowMx - 1; row >=0; row--) {
				if (row == RowMx - 1) {					
					Temp = BckRTmtrx [row - 1, column];
					BckRTmtrx [row - 1, column] = BckRTmtrx [row, column];
				} else if (row == 0) {
					BckRTmtrx [RowMx - 1, column] = Temp;
				} else {
					Temp2 = Temp;
					Temp = BckRTmtrx [row - 1, column];
					BckRTmtrx [row - 1, column] = Temp2;
				}
			}
		}
	}

	private void Move_Left(){
		RectTransform Temp = new RectTransform();
		RectTransform Temp2 = new RectTransform();
		for (int row = 0; row < RowMx; row++) {
			Vector2 TempVect = new Vector2 (
				BckRTmtrx [row, 0].anchoredPosition.x - imgW,
				BckRTmtrx [row, ColMx-1].anchoredPosition.y
			);
			BckRTmtrx [row, ColMx-1].anchoredPosition = TempVect;
		}
		for (int row = 0; row < RowMx; row++) {
			for (int column = 0; column < ColMx; column++) {
				if (column == 0) {
					Temp = BckRTmtrx [row, column + 1];
					BckRTmtrx [row, column + 1] = BckRTmtrx [row, column];
				} else if (column == ColMx - 1) {
					BckRTmtrx [row, 0] = Temp;
				} else {
					Temp2 = Temp;
					Temp = BckRTmtrx [row, column + 1];
					BckRTmtrx [row, column + 1] = Temp2;
				}
			}
		}
	}

	private void Move_Rigth(){
		RectTransform Temp = new RectTransform();
		RectTransform Temp2 = new RectTransform();
		for (int row = 0; row < RowMx; row++) {
			Vector2 TempVect = new Vector2 (
				BckRTmtrx [row, ColMx-1].anchoredPosition.x + imgW,
				BckRTmtrx [row, 0].anchoredPosition.y
			);
			BckRTmtrx [row, 0].anchoredPosition = TempVect;
		}
		for (int row = 0; row < RowMx; row++) {
			for (int column = ColMx-1; column >= 0 ; column--) {
				if (column == ColMx-1) {
					Temp = BckRTmtrx [row, column - 1];
					BckRTmtrx [row, column - 1] = BckRTmtrx [row, column];
				} else if (column == 0) {
					BckRTmtrx [row, ColMx-1] = Temp;
				} else {
					Temp2 = Temp;
					Temp = BckRTmtrx [row, column - 1];
					BckRTmtrx [row, column - 1] = Temp2;
				}
			}
		}
	}

}
