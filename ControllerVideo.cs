﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControllerVideo : MonoBehaviour {


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Publics ENUMS
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//*********************************************************
	//Engine: Spheres and general running
	//*********************************************************
	public enum SPHERE_STATE
	{
		NORMAL = 0,//Is on videos menu,it's simple
		LOAD = 1,//Is on video menu, but some tiles are modify to prepare to
		PLAY_VIDEO = 2,//Is on play the videoon the sphere
		PAUSE_VIDEO = 3,//Is when you pause the video on the sphere
		END_VIDEO = 4,//Is when the video is full 
	}
	//*********************************************************


	//*********************************************************
	//Engine: Tiles Buttons
	//*********************************************************
	public enum BUTTONS_STATE
	{
		NONE = 0,//Never has been download
		TO_DOWNLOAD = 1,//If you put the view over the element, the download logo jump to you
		ON_DOWNLOAD = 2,//If you are on an downloading proccess
		ON_STREAMING = 3,//If you are streaming proccess..
		DOWNLOAD_END = 4,//If you finish download the video and not are on this
		STREAMING_END = 5,//If you finish download the video and not are on this
		TO_PLAY_DOWNLOADED = 6,//If you are rear to play the video
		TO_PLAY_STREAMING = 7,//If you are rear to play the video
		//TO_DELETE = 8,//If you want to delete the file previous to cast
	}
	//*********************************************************


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Vars
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//*********************************************************
	//Deb options:
	public DebugScript deb;//to graphical debug
	bool DebugMode=false;//ONLY fo DEBUG in Unity SDK App, need to change to NULL the calls to loadding videos <<== XXXXXXXXXXXXXXXXXXXXXXXXXXX
	public bool JumpToForceActualState=false;
	public SPHERE_STATE ForceActualStateController;//Only For Debug working on the scene

	//Spheres:
	public GameObject VideoSphere;//Sphere of VIDEO MENU
	public GameObject PlaySphere;//Sphere for play veideos only

	//VideoTiles (as well video buttons):
	public GameObject[] VideoButtons;//Buttons of VIDEO MENU
	public GameObject VideoButtonActive;//Button press of the buttons of VIDEO MENU

	//Floor Buttons and in-play menues:
	public GameObject VideoFloorGroupButtons;//Buttons of Back and Home on VIDEO MENU - UNNESESARY
	public GameObject OnPlayFloorGroupButtons;//Buttons of Back and Home on VIDEO MENU - UNNESESARY
	public GameObject OnPlayPauseButton;//Buttons of Back and Home on VIDEO MENU - UNNESESARY
	public GameObject OnPausePlayButton;//Buttons of Back and Home on VIDEO MENU - UNNESESARY
	public GameObject OnEndRePlayButton;//Buttons of Back and Home on VIDEO MENU - UNNESESARY
	public GameObject EndPlayVideoMenu;
	public GameObject InPlayVideoMenu;

	//Internal State and Control:
	public SPHERE_STATE ActualStateController = SPHERE_STATE.NORMAL;//To Maintain the actual status of the scene
	public bool IsDownloadMode=false;//To set if you want to DOWNLOAD VIDEO FILES or anly STREAMING VIDEOS
	public bool IsForcedToStreamingMode=true;//To force change on the style if the folders are FULL's, you want to STREAMING VIDEOS only
	public string URLloadding;//Path from web to proccess
	public bool IsInLoadding;
	public string DirectoryToDownload="DownloadVideos";//The Folder to storage the files download

	//*********************************************************


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//TOOLS
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//*********************************************************
	//*********************************************************

	public string GetDirectoryPath(){
		return Application.persistentDataPath + "/" + DirectoryToDownload;
	}

	public string ConvertURLtoFileName(string URLk){
		string VideoName = URLk;
		VideoName = VideoName.Replace("http://","");
		VideoName = VideoName.Replace("/","-");
		VideoName = GetDirectoryPath() + "/" + VideoName;
		return VideoName;
	}

	public bool CheckDirectory(){
		return System.IO.Directory.Exists (GetDirectoryPath());
	}

	public void CreateDirectory(){
		if (!CheckDirectory ()) {
			System.IO.Directory.CreateDirectory(GetDirectoryPath());
		}
	}

	public void DeleteDirectory(){
		if (CheckDirectory ()) {
			System.IO.Directory.Delete(GetDirectoryPath(),true);
		}
	}

	public void ClearDirectory(){
		DeleteDirectory ();
		CreateDirectory ();
	}

	public bool FindFilesFromURL(string URLx){
		bool Existing = System.IO.File.Exists(ConvertURLtoFileName(URLx));
		return Existing;
	}

	public void DeleteFileFromURL(string URLz){
		if(FindFilesFromURL(URLz)){
			System.IO.File.Delete(ConvertURLtoFileName(URLz));
		}
	}

	private void PrepareToPlay(){
		if (deb != null)deb.AddText ("PrepareToPlay()");
		PlaySphere.SetActive (true);
		PlaySphere.GetComponent<MeshRenderer> ().enabled = false;
	}

	public void ShowVideoButtons (bool actv){
		foreach (GameObject Button in VideoButtons) {
			Button.SetActive (actv);
		}
	}

	public void ReplaceButtonStates(BUTTONS_STATE ToChange, BUTTONS_STATE CorrectValue){
		foreach (GameObject Button in VideoButtons) {
			if (Button.GetComponent<SelectItem> ().StateOfButton == ToChange) {
				Button.GetComponent<SelectItem> ().StateOfButton = CorrectValue;
			}
		}
	}

	public GameObject GetObjectButtonByURL(string URLtf){
		foreach (GameObject Buttn in VideoButtons) {
			if (Buttn.GetComponent<VideoURL> ().URL == URLtf) {
				return Buttn;
			}
		}
		return null;
	}

	public void PlayProccess(){
		ActualStateController = SPHERE_STATE.PLAY_VIDEO;
		SetSphereAmbient ();
		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().Play ();
	}

	public void ReticleBetweenScenes(bool act){
		if (VideoButtonActive != null) {
			if (VideoButtonActive.GetComponent<SelectItem> ().Reticle != null) {
				VideoButtonActive.GetComponent<SelectItem> ().Reticle.GetComponent<MeshRenderer> ().enabled = act;
			}
		}
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//State Machine of Spheres and general running
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//*********************************************************
	//*********************************************************

	private void SetSphereAmbient(){
		
		switch (ActualStateController) {
		case SPHERE_STATE.NORMAL:

			VideoSphere.SetActive (true);
			VideoSphere.GetComponent<MeshRenderer> ().enabled = true;
			PlaySphere.SetActive (false);

			ShowVideoButtons (true);
			RefrshButtons ();
			ShowVideoButtons (true);

			VideoFloorGroupButtons.SetActive(true);
			OnPlayFloorGroupButtons.SetActive(false);
			OnPlayPauseButton.SetActive(false);
			OnPausePlayButton.SetActive(false);
			OnEndRePlayButton.SetActive(false);
			EndPlayVideoMenu.SetActive(false);
			InPlayVideoMenu.SetActive(false);

			break;
		case SPHERE_STATE.LOAD:

			VideoSphere.SetActive (true);
			VideoSphere.GetComponent<MeshRenderer> ().enabled = true;
			PlaySphere.SetActive (true);
			PlaySphere.GetComponent<MeshRenderer> ().enabled = false;

			ShowVideoButtons (true);
			RefrshButtons ();
			ShowVideoButtons (true);

			VideoFloorGroupButtons.SetActive(true);
			OnPlayFloorGroupButtons.SetActive(false);
			OnPlayPauseButton.SetActive(false);
			OnPausePlayButton.SetActive(false);
			OnEndRePlayButton.SetActive(false);
			EndPlayVideoMenu.SetActive(false);
			InPlayVideoMenu.SetActive(false);

			break;
		case SPHERE_STATE.PLAY_VIDEO:

			VideoSphere.SetActive (false);
			PlaySphere.SetActive (true);
			PlaySphere.GetComponent<MeshRenderer> ().enabled = true;

			ShowVideoButtons (false);

			VideoFloorGroupButtons.SetActive(false);
			OnPlayFloorGroupButtons.SetActive(true);
			OnPlayPauseButton.SetActive(true);
			OnPausePlayButton.SetActive(false);
			OnEndRePlayButton.SetActive(false);
			EndPlayVideoMenu.SetActive(false);
			InPlayVideoMenu.SetActive(false);

			break;
		case SPHERE_STATE.PAUSE_VIDEO:

			VideoSphere.SetActive (false);
			PlaySphere.SetActive (true);
			PlaySphere.GetComponent<MeshRenderer> ().enabled = true;

			ShowVideoButtons (false);

			VideoFloorGroupButtons.SetActive(false);
			OnPlayFloorGroupButtons.SetActive(true);
			OnPlayPauseButton.SetActive(false);
			OnPausePlayButton.SetActive(true);
			OnEndRePlayButton.SetActive(false);
			EndPlayVideoMenu.SetActive(false);
			InPlayVideoMenu.SetActive(true);

			break;
		case SPHERE_STATE.END_VIDEO:
			
			VideoSphere.SetActive (false);
			PlaySphere.SetActive (true);
			PlaySphere.GetComponent<MeshRenderer> ().enabled = true;

			ShowVideoButtons (false);

			VideoFloorGroupButtons.SetActive(false);
			OnPlayFloorGroupButtons.SetActive(true);
			OnPlayPauseButton.SetActive(false);
			OnPausePlayButton.SetActive(false);
			OnEndRePlayButton.SetActive(true);
			EndPlayVideoMenu.SetActive(true);
			InPlayVideoMenu.SetActive(false);

			break;
		}

	}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//State Machine of Tiles Buttons
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//*********************************************************
	//*********************************************************

	public void SetVideoButton(GameObject Button, BUTTONS_STATE stateButton){
		switch(stateButton){
		case BUTTONS_STATE.TO_DOWNLOAD: //Case 1: Show the download button...
			Button.GetComponent<SelectItem> ().StateOfButton=BUTTONS_STATE.TO_DOWNLOAD;

			Button.GetComponent<SelectItem> ().DownImage.SetActive(true);
			Button.GetComponent<SelectItem> ().LoaddingText.SetActive(false);
			Button.GetComponent<SelectItem> ().LoaddingCircle.SetActive(false);

			Button.GetComponent<SelectItem> ().TrashButton.SetActive(false);
			Button.GetComponent<SelectItem> ().TrashButton_Blank.SetActive(true);
		
			Button.GetComponent<SelectItem> ().PlayButton.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOff.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOn.SetActive(false);

			break;
		case BUTTONS_STATE.ON_DOWNLOAD: //Case 2: Is on download to an file on the phone...
			Button.GetComponent<SelectItem> ().StateOfButton=BUTTONS_STATE.ON_DOWNLOAD;

			Button.GetComponent<SelectItem> ().DownImage.SetActive(true);
			Button.GetComponent<SelectItem> ().LoaddingText.SetActive(true);
			Button.GetComponent<SelectItem> ().LoaddingCircle.SetActive(true);

			Button.GetComponent<SelectItem> ().TrashButton.SetActive(false);
			Button.GetComponent<SelectItem> ().TrashButton_Blank.SetActive(true);

			Button.GetComponent<SelectItem> ().PlayButton.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOff.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOn.SetActive(false);

			Button.GetComponent<SelectItem> ().LoaddingCircle.GetComponent<ProgressCircAutom3D> ().IsDownloadModeCircle = true;

			break;
		case BUTTONS_STATE.ON_STREAMING: //Case 3: Is load to MPC player...
			Button.GetComponent<SelectItem> ().StateOfButton=BUTTONS_STATE.ON_STREAMING;

			Button.GetComponent<SelectItem> ().DownImage.SetActive(true);
			Button.GetComponent<SelectItem> ().LoaddingText.SetActive(true);
			Button.GetComponent<SelectItem> ().LoaddingCircle.SetActive(true);

			Button.GetComponent<SelectItem> ().TrashButton.SetActive(false);
			Button.GetComponent<SelectItem> ().TrashButton_Blank.SetActive(true);

			Button.GetComponent<SelectItem> ().PlayButton.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOff.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOn.SetActive(false);

			Button.GetComponent<SelectItem> ().LoaddingCircle.GetComponent<ProgressCircAutom3D> ().IsDownloadModeCircle = false;
			Button.GetComponent<SelectItem> ().LoaddingText.GetComponent<TextMesh>().text = LoadText;

			break;
		case BUTTONS_STATE.DOWNLOAD_END: //Case 4: Is available to play... from download
			Button.GetComponent<SelectItem> ().StateOfButton = BUTTONS_STATE.DOWNLOAD_END;

			Button.GetComponent<SelectItem> ().DownImage.SetActive (false);
			Button.GetComponent<SelectItem> ().LoaddingText.SetActive (false);
			Button.GetComponent<SelectItem> ().LoaddingCircle.SetActive (false);

			Button.GetComponent<SelectItem> ().TrashButton.SetActive (true);//normalstate
			Button.GetComponent<SelectItem> ().TrashButton_Blank.SetActive (false);
			SetDeletButtonShow (Button, 2);

			Button.GetComponent<SelectItem> ().PlayButton.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOff.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOn.SetActive(true);

			break;
		case BUTTONS_STATE.STREAMING_END: //Case 5: Is available to play... from streaming
			Button.GetComponent<SelectItem> ().StateOfButton=BUTTONS_STATE.STREAMING_END;

			Button.GetComponent<SelectItem> ().DownImage.SetActive(false);
			Button.GetComponent<SelectItem> ().LoaddingText.SetActive(false);
			Button.GetComponent<SelectItem> ().LoaddingCircle.SetActive(false);

			Button.GetComponent<SelectItem> ().TrashButton.SetActive(false);
			Button.GetComponent<SelectItem> ().TrashButton_Blank.SetActive(true);

			Button.GetComponent<SelectItem> ().PlayButton.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOff.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOn.SetActive(true);

			break;
		case BUTTONS_STATE.TO_PLAY_DOWNLOADED: //Case 6: Is copy files or prepare to play the video!!!
			Button.GetComponent<SelectItem> ().StateOfButton=BUTTONS_STATE.TO_PLAY_DOWNLOADED;

			Button.GetComponent<SelectItem> ().DownImage.SetActive(false);
			Button.GetComponent<SelectItem> ().LoaddingText.SetActive(false);
			Button.GetComponent<SelectItem> ().LoaddingCircle.SetActive(false);

			Button.GetComponent<SelectItem> ().TrashButton.SetActive(true);//normalstate
			Button.GetComponent<SelectItem> ().TrashButton_Blank.SetActive(false);

			Button.GetComponent<SelectItem> ().PlayButton.SetActive(true);
			Button.GetComponent<SelectItem> ().OkButtonOff.SetActive(true);
			Button.GetComponent<SelectItem> ().OkButtonOn.SetActive(false);

			break;
		case BUTTONS_STATE.TO_PLAY_STREAMING: //Case 7: Is copy files or prepare to play the video!!!
			Button.GetComponent<SelectItem> ().StateOfButton=BUTTONS_STATE.TO_PLAY_STREAMING;

			Button.GetComponent<SelectItem> ().DownImage.SetActive(false);
			Button.GetComponent<SelectItem> ().LoaddingText.SetActive(false);
			Button.GetComponent<SelectItem> ().LoaddingCircle.SetActive(false);

			Button.GetComponent<SelectItem> ().TrashButton.SetActive(false);
			Button.GetComponent<SelectItem> ().TrashButton_Blank.SetActive(true);

			Button.GetComponent<SelectItem> ().PlayButton.SetActive(true);
			Button.GetComponent<SelectItem> ().OkButtonOff.SetActive(true);
			Button.GetComponent<SelectItem> ().OkButtonOn.SetActive(false);

			break;
		case BUTTONS_STATE.NONE: //Case 0: Never has been download...
			Button.GetComponent<SelectItem> ().StateOfButton=BUTTONS_STATE.NONE;

			Button.GetComponent<SelectItem> ().DownImage.SetActive(false);
			Button.GetComponent<SelectItem> ().LoaddingText.SetActive(false);
			Button.GetComponent<SelectItem> ().LoaddingCircle.SetActive(false);

			Button.GetComponent<SelectItem> ().TrashButton.SetActive(false);
			Button.GetComponent<SelectItem> ().TrashButton_Blank.SetActive(true);

			Button.GetComponent<SelectItem> ().PlayButton.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOff.SetActive(false);
			Button.GetComponent<SelectItem> ().OkButtonOn.SetActive(false);

			break;
		}
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// >>> DOWNLOAD File Proccess <<<
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	public void DownloadFromWeb(GameObject buttonPres){
		buttonPres.GetComponent<DownloadVideoProccess>().DownloadFromWeb();

	}

	public void PassPathToLoadMPC(string url2){
		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().Load ("file://"+ConvertURLtoFileName(url2));//XXXXXXXXXXXXXXXXXXXXXXXXXXX
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// >>> STREAMING URL Proccess <<<
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//*********************************************************
	public string LoadText="Loadding...";
	public bool LoaddingEnd;
	//*********************************************************

	public void CancellOtherStream(){
		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().UnLoad();
	}

	public void StreamingFromWebEnd(){
		IsInLoadding = false;
		SetVideoButton (VideoButtonActive, BUTTONS_STATE.STREAMING_END);
	}

	public void StreamingFromWeb(string url3){
		if (deb != null)deb.AddText ("StreamingFromWeb( "+ url3+" )");

		SetVideoButton (VideoButtonActive, BUTTONS_STATE.ON_STREAMING);
		IsInLoadding = true;
		CancellOtherStream ();
		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().Load (url3);
	}

	public bool IsLoadedideo() {
		switch (PlaySphere.GetComponent<MediaPlayerCtrl> ().m_CurrentState) {
		case MediaPlayerCtrl.MEDIAPLAYER_STATE.READY:
			return true;
			break;
		case MediaPlayerCtrl.MEDIAPLAYER_STATE.END:
			return true;
			break;
		case MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED:
			return true;
			break;
		case MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING:
			return true;
			break;
		case MediaPlayerCtrl.MEDIAPLAYER_STATE.STOPPED:
			return true;
			break;
		case MediaPlayerCtrl.MEDIAPLAYER_STATE.ERROR:
			return false;
			break;
		case MediaPlayerCtrl.MEDIAPLAYER_STATE.NOT_READY:
			return false;
			break;
		default:
			return false;
			break;
		}
	}

	public void UpdateStreaming (){
		if (!DebugMode)LoaddingEnd = IsLoadedideo ();
		if (LoaddingEnd) {
			StreamingFromWebEnd ();
		}

	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Event Collectors
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//*********************************************************
	//*********************************************************

	//*********************************************************
	// On Video Events
	//*********************************************************

	public void RefrshButtons () {
		if (deb != null)deb.AddText ("RefrshButtons()...");

		VideoButtons = GameObject.FindGameObjectsWithTag ("Video");
		if (deb != null)deb.AddText ("VidoButtons["+VideoButtons.Length+"]="+VideoButtons);

		foreach (GameObject Buttn in VideoButtons) {
			if (deb != null)deb.AddText ("---->Path+URL To Check:"+ConvertURLtoFileName(Buttn.GetComponent<VideoURL> ().URL));
			if (deb != null)deb.AddText ("---->Result: "+FindFilesFromURL (Buttn.GetComponent<VideoURL> ().URL));

			if (FindFilesFromURL (Buttn.GetComponent<VideoURL> ().URL)) {
				//Case 4: Is available to play...
				SetVideoButton(Buttn, BUTTONS_STATE.DOWNLOAD_END);
			} else {
				SetVideoButton(Buttn, Buttn. GetComponent<SelectItem> ().StateOfButton);
			}
		}
	}

	public void Video_Press (GameObject button){//Its call from a video button to loadding or play
		if (deb != null)deb.AddText ("Video_Press...");
		button.GetComponent<SelectItem> ().elapsedTime = 0f;

		if (button.GetComponent<SelectItem> ().StateOfButton == BUTTONS_STATE.NONE) {
			if (deb != null)deb.AddText ("Exception...");
			SetVideoButton(button, BUTTONS_STATE.TO_DOWNLOAD);
		}

		if (VideoButtonActive != button) {
			if (deb != null)deb.AddText ("Intro normal or exception...");

			VideoButtonActive = button;
			URLloadding = VideoButtonActive.GetComponent<VideoURL> ().URL;
			if (VideoButtonActive.GetComponent<SelectItem> ().StateOfButton == BUTTONS_STATE.TO_PLAY_DOWNLOADED) {
				//Promove to play a downloaded video
				PassPathToLoadMPC (URLloadding);
				PlayProccess ();
			} else {
				ActualStateController = SPHERE_STATE.LOAD;
				SetSphereAmbient ();
				if (!IsForcedToStreamingMode && IsDownloadMode) {
					VideoButtonActive.GetComponent<SelectItem> ().StateOfButton = BUTTONS_STATE.ON_DOWNLOAD;
					RefrshButtons ();
					DownloadFromWeb (button);
				} else {
					VideoButtonActive.GetComponent<SelectItem> ().StateOfButton = BUTTONS_STATE.ON_STREAMING;
					RefrshButtons ();
					StreamingFromWeb (URLloadding);
				}
			}
		} else {
			if (VideoButtonActive.GetComponent<SelectItem> ().StateOfButton == BUTTONS_STATE.TO_PLAY_DOWNLOADED) {
				//Promove to play a downloaded video an local video from disk?
				ActualStateController = SPHERE_STATE.LOAD;
				SetSphereAmbient ();
				PassPathToLoadMPC(URLloadding);
				ReticleBetweenScenes (false);
				PlayProccess ();
			}else{
				if (VideoButtonActive.GetComponent<SelectItem> ().StateOfButton == BUTTONS_STATE.TO_PLAY_STREAMING) {
					PlayProccess ();
				} else {
					if (VideoButtonActive.GetComponent<SelectItem> ().StateOfButton == BUTTONS_STATE.TO_DOWNLOAD) {
						ActualStateController = SPHERE_STATE.LOAD;
						SetSphereAmbient ();
						if (!IsForcedToStreamingMode && IsDownloadMode) {
							VideoButtonActive.GetComponent<SelectItem> ().StateOfButton = BUTTONS_STATE.ON_DOWNLOAD;
							RefrshButtons ();
							DownloadFromWeb (button);
						} else {
							VideoButtonActive.GetComponent<SelectItem> ().StateOfButton = BUTTONS_STATE.ON_STREAMING;
							RefrshButtons ();
							StreamingFromWeb (URLloadding);
						}

					}
				}
			}
		}
	}

	public void DeleteVideo_Press (GameObject button){
		if (deb != null)deb.AddText ("DELETE VIDEO ORDER!!!!!!!!");
		button.GetComponent<SelectItem> ().elapsedTime = 0f;
		DeleteFileFromURL(button.GetComponent<VideoURL> ().URL);
		button.GetComponent<SelectItem> ().StateOfButton = BUTTONS_STATE.NONE;
		RefrshButtons ();
	}
		
	public void SetDeletButtonShow(GameObject button, int CaseOf){
		switch (CaseOf) {
		case 0: //Is on Play Button and has downloadding (need to show in withe)
			button.GetComponent<SelectItem>().TrashButton.GetComponent<SelectItem>().Sprite_On.SetActive (false);
			button.GetComponent<SelectItem>().TrashButton.GetComponent<SelectItem>().Sprite_Off.SetActive (true);
			button.GetComponent<SelectItem>().TrashButton.GetComponent<SelectItem>().Sprite_NO.SetActive (false);
			break;
		case 1: //Is on delete button (need to show in blue)
			button.GetComponent<SelectItem>().TrashButton.GetComponent<SelectItem>().Sprite_On.SetActive (true);
			button.GetComponent<SelectItem>().TrashButton.GetComponent<SelectItem>().Sprite_Off.SetActive (false);
			button.GetComponent<SelectItem>().TrashButton.GetComponent<SelectItem>().Sprite_NO.SetActive (false);
			break;
		default: //Is no on the button and need to hide...
			Debug.Log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
			button.GetComponent<SelectItem>().TrashButton.GetComponent<SelectItem>().Sprite_On.SetActive (false);
			button.GetComponent<SelectItem>().TrashButton.GetComponent<SelectItem>().Sprite_Off.SetActive (false);
			button.GetComponent<SelectItem>().TrashButton.GetComponent<SelectItem>().Sprite_NO.SetActive (true);
			break;
		}
	}

	public void Delete_OnGazeEnter (GameObject button){
		SetVideoButton(button, BUTTONS_STATE.TO_PLAY_DOWNLOADED);
		SetDeletButtonShow (button, 1);
	}

	public void Delete_OnGazeExit (GameObject button){
		SetVideoButton(button, BUTTONS_STATE.DOWNLOAD_END);
	}

	public void Video_OnGazeEnter (GameObject button){
		if (deb != null)deb.AddText ("GazeEnter (GameObject button)..............");

		switch (button.GetComponent<SelectItem> ().StateOfButton) {
		case BUTTONS_STATE.NONE:
			SetVideoButton(button, BUTTONS_STATE.TO_DOWNLOAD);
			break;
		case BUTTONS_STATE.DOWNLOAD_END:
			SetVideoButton (button, BUTTONS_STATE.TO_PLAY_DOWNLOADED);
			SetDeletButtonShow (button, 0);
			break;
		case BUTTONS_STATE.STREAMING_END:
			SetVideoButton(button, BUTTONS_STATE.TO_PLAY_STREAMING);
			break;
		}
	}

	public void Video_OnGazeExit (GameObject button){
		if (deb != null)deb.AddText ("GazeExit (GameObject button)!!!!");

		switch (button.GetComponent<SelectItem> ().StateOfButton) {
		case BUTTONS_STATE.TO_DOWNLOAD:
			SetVideoButton(button, BUTTONS_STATE.NONE);
			break;
		case BUTTONS_STATE.TO_PLAY_DOWNLOADED:
			SetVideoButton(button, BUTTONS_STATE.DOWNLOAD_END);
			break;
		case BUTTONS_STATE.TO_PLAY_STREAMING:
			SetVideoButton(button, BUTTONS_STATE.STREAMING_END);
			break;
		}

	}


	//*********************************************************
	// On Play Events
	//*********************************************************

	public void Pause_Press(GameObject button){
		if (deb != null)deb.AddText ("Pause_Press()..............");
		button.GetComponent<SelectItem> ().elapsedTime = 0f;

		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().Pause ();
		ActualStateController = SPHERE_STATE.PAUSE_VIDEO;
		SetSphereAmbient ();

	}

	public void RePlay_Press(GameObject button){
		if (deb != null)deb.AddText ("RePlay_Press()..............");
		button.GetComponent<SelectItem> ().elapsedTime = 0f;
		ActualStateController = SPHERE_STATE.PLAY_VIDEO;
		SetSphereAmbient ();
		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().SeekTo(0);
		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().Play ();

	}

	public void Play_Press(GameObject button){
		if (deb != null)deb.AddText ("Play_Press()..............");
		button.GetComponent<SelectItem> ().elapsedTime = 0f;
		ActualStateController = SPHERE_STATE.PLAY_VIDEO;
		SetSphereAmbient ();
		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().Play ();

	}

	public void Rewind_Press(GameObject button){
		if (deb != null)deb.AddText ("Rewind_Press()..............");
		button.GetComponent<SelectItem> ().elapsedTime = 0f;
		ActualStateController = SPHERE_STATE.PAUSE_VIDEO;
		SetSphereAmbient ();
		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().Pause ();
		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().SeekTo(0);
	}

	public void BackVideos_Press(GameObject button){
		if (deb != null)deb.AddText ("BackVideos_Press()..............");
		button.GetComponent<SelectItem> ().elapsedTime = 0f;

		if(!DebugMode)PlaySphere.GetComponent<MediaPlayerCtrl> ().Stop();
		ActualStateController = SPHERE_STATE.LOAD;
		SetSphereAmbient ();
		ReticleBetweenScenes (true);
	}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Start and Update
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	//*********************************************************
	//*********************************************************

	// Use this for initialization
	void Start () {
		IsInLoadding=false;
		LoaddingEnd=false;
		CreateDirectory ();
		ActualStateController = SPHERE_STATE.NORMAL;
		SetSphereAmbient ();
		ReticleBetweenScenes (true);
		RefrshButtons ();
	}

	void Update () {
		if (IsInLoadding) {
			if (!IsForcedToStreamingMode && IsDownloadMode) {
			} else {
				UpdateStreaming ();
			}
		}

		if (ActualStateController == SPHERE_STATE.PLAY_VIDEO) {
			switch(PlaySphere.GetComponent<MediaPlayerCtrl>().m_CurrentState){
			case MediaPlayerCtrl.MEDIAPLAYER_STATE.END:
				ActualStateController = SPHERE_STATE.END_VIDEO;
				SetSphereAmbient ();
				break;
			case MediaPlayerCtrl.MEDIAPLAYER_STATE.READY:
				PlaySphere.GetComponent<MediaPlayerCtrl> ().Play ();
				break;
			case MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING:
				if (EndPlayVideoMenu.activeSelf) {
					EndPlayVideoMenu.SetActive (false);
				}
				if (InPlayVideoMenu.activeSelf) {
					InPlayVideoMenu.SetActive (false);
				}
				break;
			}
		}

		if ((ActualStateController != ForceActualStateController)&&(JumpToForceActualState)) {
			ActualStateController = ForceActualStateController;
			SetSphereAmbient ();
			JumpToForceActualState = false;
		}
	}
}
